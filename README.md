# Setting up Tailwind CSS

**Note**: This will be setup with [Nuxt.js](https://nuxtjs.org/).

1. Create a new directory for the project.

   ```bash
   mkdir new-project
   ```

2. Change directory and initialise the project.

   ```bash
   git init
   yarn init -y
   echo node_modules >> .gitignore
   ```

3. Install `nuxt` as a dependency.

   ```bash
   yarn add nuxt
   ```

4. Configure `package.json`'s scripts:

   ```json
     "scripts": {
       "dev": "nuxt",
       "build": "nuxt build",
       "generate": "nuxt generate",
       "start": "nuxt start"
     },
   ```

5. Install `@nuxtjs/tailwindcss`, `tailwindcss@latest`, `postcss@latest` and `autoprefixer@latest` as dev dependencies:

   ```bash
   yarn add -D @nuxtjs/tailwindcss tailwindcss@latest postcss@latest autoprefixer@latest
   ```

6. Initialise tailwind in your project:

   ```bash
   npx tailwindcss init
   ```

7. Add the following to `tailwind.config.js` under the `purge` field:

   ```js
   module.exports = {
     purge: [
       "./components/**/*.{vue,js}",
       "./layouts/**/*.vue",
       "./pages/**/*.vue",
       "./plugins/**/*.{js,ts}",
       "./nuxt.config.{js,ts}",
     ],
     ...
   }
   ```

8. Add the following to `nuxt.config.js` under the `buildModules` field:

   ```js
   module.exports = {
     buildModules: ["@nuxtjs/tailwindcss"],
   };
   ```

9. Create a `tailwind.css` file in `/assets/css`

   ```bash
   touch assets/css/tailwind.css
   ```

10. Import the following using the `@tailwindcss` directive in `tailwind.css`:

    ```css
    @tailwind base;
    @tailwind components;
    @tailwind utilities;
    ```

11. You can now use tailwind in your project - create an index page:

    ```bash
    touch pages/index.vue
    ```

    And add the following:

    ```html
    <template>
      <div class="max-w-md mx-auto bg-white rounded-xl shadow-md p-8  m-8">
        <p class="text-xl font-medium text-black">Tailwind</p>
        <p class="text-purple-900 text-lg">Tailwind with Nuxt.JS!</p>
      </div>
    </template>
    ```
